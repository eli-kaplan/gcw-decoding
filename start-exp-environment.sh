#!/usr/bin/env bash
echo ""
echo "==> Building the experiment environment docker image... <=="
echo ""
docker build -f Dockerfile -t gcw-decoding-exp .

WORK_DIR=$(pwd)

# Start container
echo ""
echo "==> Booting up the experiment envrionment... all the experiment code should be in the current working directory. <=="
echo ""
echo "To run experiment 1: ./run-experiment-1.sh"
echo "To run experiment 2: ./run-experiment-2.sh"
echo "See README.md for more details."
docker run --rm -p 8888:8888 -e "XLA_FLAGS=--xla_hlo_profile" -e "TF_XLA_FLAGS=--tf_xla_cpu_global_jit" -v $WORK_DIR:/work -it gcw-decoding-exp