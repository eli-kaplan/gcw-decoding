"""
Program     data-gen-common.py
Purpose     Data generation code common to both Experiment 1 and Experiment 2
Author      Eli Kaplan
Date        Apr 2020
"""

from RqaPython.Rqa import Rqa
import multiprocessing
import traceback
import numpy as np
import sys
import os
import json

def do_rqa_generation(radius, params, fixations):
    """ Runs RQA with given radius and parameters on a specified fixation sequence
    
    TODO(eli-kaplan): Add option to incorporate fixation time data (making fixations 3xN instead of 2xN array)

    Arguments:
        radius {int} -- Recurrence radius
        params {dict} -- Common RQA parameters (see RqaPython)
        fixations {list([x,y])} -- Fixation sequence data
    
    Returns:
        (int, dict) -- (Recurrence radius, RQA results)
    """
    rqa_params = params
    rqa_params['radius'] = radius # Insert radius into parameters

    result = Rqa(fixations, rqa_params)

    # Return results keyed with radius (to facilitate parallelization)
    return (radius, result)


class FixationDataGenerator:
    """ Class for processing raw fixation data and generating appropriately formatted data to plug into the network.
        This class contains the setup / processing code common to both Experiment 1 and Experiment 2. Each experiment implements its own
        parse_data_from_file() and gen_label(), as these are specific to the file format and the specifics of the labels being generated.
    """
    target_files = []               # List of files to load data from
    target_rqa_radii = []           # List of analytical radii to run RQA at
    common_rqa_parameters = {}      # Common parameters passed to Anderson et al. (2013) RQA code
    base_output_directory = None    # Output directory to store results folder in
    debug = False                   # Enable debugging output?

    loaded_data = {}                # Internal dict holding runtime-loaded data

    random_control_labels = False   # Generate random control labels?

    def __init__(self, files, radii, params, output_dir, debug=False):
        """ Constructor
        
        Arguments:
            files {list(string)} -- List of filenames to load raw data from
            radii {list(int)} -- List of radii to run RQA at for each file
            params {dict} -- Common parameters to provide to each RQA run (see RqaPython)
            output_dir {string} -- Base path to output processed datasets under
        
        Keyword Arguments:
            debug {bool} -- Enable debugging output? (default: {False})
        """
        self.target_files = files
        self.target_rqa_radii = radii
        self.common_rqa_parameters = params
        self.debug = debug
        self.base_output_directory = output_dir

    def __del__(self):
        """ Destructor
        """
        return

    def parse_data_from_file(self, filename):
        raise Exception("parse_data_from_file() must be implemented per-experiment.")

    def gen_label(self, label_data, label_type="categorical"):
        raise Exception("gen_label() must be implemented per-experiment.")

    def load_all_source_data(self):
        """ Loads source data from all specified files into memory
        """
        for file in self.target_files:
            self.parse_data_from_file(file)

    def process_trial_parallel(self, thread_pool, trial_data, data_type="rqa", label_type="continuous"):
        """ Produces processed data for a given file, and generates the associated label. 

        Uses the specified Multiprocessing thread pool to greatly expedite data processing.

        When "rqa" is specified as the data type, performs RQA at each specified analytical radius (self.target_rqa_radii), and
        produces an output data "frame" as a 12xR array, where R is the number of RQA radii being tried. 
        
        Arguments:
            thread_pool {multiprocessing.Pool} -- Thread pool to use
            trial_data {dict} -- Loaded per-trial data - see parse_data_from_file()
        
        Keyword Arguments:
            data_type {str} -- Type of output data to produce - "rqa" produces per-radius RQA matrix, more options TBD (default: {"rqa"})
            label_type {str} -- Type of label to output - "continuous" or "categorical" (default: {"continuous"})

        Returns:
            (label, data) {np.array, np.array) -- Label and processed data
        """
        # Retrieve the fixation sequence and true window size
        fixations = trial_data["fixations"]
        window = trial_data["window"]

        label = []
        data = []

        # Generate the proper label for this trial
        label = self.gen_label(window, label_type)

        # Process the data frame according to specified data_type
        if data_type == "rqa":
            # Perform RQA on this trial at all specified analytical radii
            all_rqa_results = thread_pool.starmap(do_rqa_generation, 
                [(x, self.common_rqa_parameters, fixations) for x in self.target_rqa_radii])

            # Parse the output of each RQA run into their respective rows in the output data
            # For more details on what each field of the RQA results represents, see the original RQA paper.
            for raw_result in all_rqa_results:
                output_row = [
                    raw_result[0],              # Radius input to RQA
                    raw_result[1]['nrec'],      # Number of recurrences
                    raw_result[1]['rec'],       # Recurrence percentage
                    raw_result[1]['det'],       # Determinism
                    raw_result[1]['meanline'],  # Meanline
                    raw_result[1]['maxline'],   # Maxline
                    raw_result[1]['ent'],       # Line length entropy
                    raw_result[1]['relent'],    # Relative line length entropy
                    raw_result[1]['lam'],       # Laminarity
                    raw_result[1]['tt'],        # TT
                    raw_result[1]['corm'],      # Center of recurrence mass
                    raw_result[1]['clusters'],  # Number of recurrence clusters
                ]
                data.append(output_row)

            # Convert the output data frame to a Numpy array, and convert any nans to zeroes
            data = np.nan_to_num(np.array(data))

        else:
            raise Exception("Unimplemented: non-rqa data format generation")

        return label, data


    def process_all_data(self, output_name, data_type="rqa", label_type="continuous", test_portion=0.25):
        """ Processes all loaded trial data according to the specified parameters, and outputs the results in the specified folder.
        
        Outputs resulting data according to the following scheme:
            <trial>.data.npy contains the output data for each trial
            <trial>.label.npy contains the output label for each trial
            meta.json contains dataset metadata including train-test split, data/label type, radii tested, etc. 

        Arguments:
            output_name {str} -- Name of output dataset - stored as subdirectory under self.base_output_directory
        
        Keyword Arguments:
            data_type {str} -- Type of output data to produce - "rqa" produces per-radius RQA matrix, more options TBD (default: {"rqa"})
            label_type {str} -- Type of label to output - "continuous" or "categorical" (default: {"continuous"})
            test_portion {float} -- Portion of dataset to use for testing (default: {0.25})
        """
        # Make sure raw source data is loaded
        if not self.loaded_data:
            raise Exception("Need to load data before data can be processed... see load_all_source_data()")

        if self.debug:
            print("Processing data to generate {} frames, with {} labels... (randomized labels: {})".format(data_type, label_type, self.random_control_labels))

        # Initialize a thread pool for calculations
        calculation_thread_pool = multiprocessing.Pool()

        metadata = {
            "train_trials": [],
            "test_trials": [],
            "data_type": data_type,
            "label_type": label_type,
            "rqa_radii": self.target_rqa_radii,
            "rqa_params": self.common_rqa_parameters,
            "raw_files_used": self.target_files,
        }

        # Create necessary directory structure if it does not already exist
        output_path = self.base_output_directory + output_name
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        
        # Generate an "index" for each file to prepend to each trial ID, to prevent overlap
        file_indices = {}
        cur_file = 1
        for file in self.loaded_data.keys():
            file_indices[file] = cur_file * 10000
            cur_file += 1

        # Build a list of all loaded trial IDs
        all_trials = []
        for file in self.loaded_data.keys(): # Iterate through all loaded files
            for trial in self.loaded_data[file].keys(): # Iterate through all trials in this file
                trial_idx = file_indices[file] + int(trial) # Generate a unique index for this trial (file_idx + trial_idx)
                all_trials.append(trial_idx)

        # Shuffle the trial IDs to randomize split (i.e. don't accidentally split one file as all training data and another file as all testing data)
        np.random.shuffle(all_trials)

        # Generate a train-test split, and store the selected indices in the metadata
        num_train = int(len(all_trials) * (1 - test_portion))

        train_trials = all_trials[:num_train]
        test_trials = all_trials[num_train:]

        metadata["train_trials"] = train_trials
        metadata["test_trials"] = test_trials

        # Define an internal helper method to retrieve trial data by its unique ID
        def _trial_data_by_ID(idx):
            index_in_file = (idx % 10000)
            file_index = (idx - index_in_file) // 10000
            
            file = list(self.loaded_data.keys())[file_index - 1]
            trial = list(self.loaded_data[file].keys())[index_in_file - 1]
            return self.loaded_data[file][trial]

        num_trials_processed = 0
        # Iterate through and process all data, storing each processed trial in its respective output file
        for trial_id in all_trials:
            if self.debug and num_trials_processed % 20 == 0:
                print("{}...".format(num_trials_processed))

            num_trials_processed += 1

            # Get the raw data for this trial
            trial_raw_data = _trial_data_by_ID(trial_id)

            # Perform the RQA calculations for this trial
            label, data = self.process_trial_parallel(calculation_thread_pool, trial_raw_data, data_type=data_type, label_type=label_type)

            # Figure out where to save this trial's output data
            data_destination = "{}/{}.data.npy".format(output_path, trial_id)
            label_destination = "{}/{}.label.npy".format(output_path, trial_id)

            # Save this processed trial to the disk
            np.save(label_destination, label)
            np.save(data_destination, data)

        # Finally, save the metadata
        metadata_path = "{}/meta.json".format(output_path)

        with open(metadata_path, "w") as metadata_file:
            metadata_file.write(json.dumps(metadata))

        print("Generated data for {} training and {} test trials ({}).".format(len(train_trials), len(test_trials), output_path))
