#!/usr/bin/env bash
# run-experiment-1.sh : Runs experiment 1 a specified number of times, storing train/test results in exp-results/exp1.# where # is the run number

NUM_EXPERIMENT_RUNS=10
EPOCHS_PER_RUN=15

# Clean up old experimental results
rm exp-results/exp1-*.txt 

# Run the experiment the specified number of times
echo "Running $NUM_EXPERIMENT_RUNS runs of experiment 1, training for $EPOCHS_PER_RUN epochs each trial."
for i in $(seq 1 $NUM_EXPERIMENT_RUNS)
do
    echo "Generating data for trial $i..."
    rm processed-data/exp1-* > /dev/null 2>&1
    ./data_gen_exp1.py > /dev/null 2>&1

    echo "Running prediction for trial $i - true labels, both continuous and categorical..."
    echo "-- True Labels (Continuous) --" >> exp-results/exp1-$i.txt
    ./predict.py processed-data/exp1-continuous $EPOCHS_PER_RUN >> exp-results/exp1-$i.txt 2>/dev/null
    echo "-- True Labels (Categorical) --" >> exp-results/exp1-$i.txt
    ./predict.py processed-data/exp1-categorical $EPOCHS_PER_RUN >> exp-results/exp1-$i.txt 2>/dev/null
    
    echo "Running prediction for trial $i - control (random) labels, both continuous and categorical..."
    echo "-- Control Labels (Continuous) --" >> exp-results/exp1-$i.txt
    ./predict.py processed-data/exp1-continuous-validation $EPOCHS_PER_RUN >> exp-results/exp1-$i.txt 2>/dev/null
    echo "-- Control Labels (Categorical) --" >> exp-results/exp1-$i.txt
    ./predict.py processed-data/exp1-categorical-validation $EPOCHS_PER_RUN >> exp-results/exp1-$i.txt 2>/dev/null
done
echo "All done! See exp-results/exp1-* for results from each run."