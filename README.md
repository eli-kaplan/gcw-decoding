
# GCW-Decoding

*Code for analyzing eye-tracking fixation recurrence patterns under the gaze-contingent windowing experimental paradigm. Applies a decoding-style approach to see if the size of the window (and further, whether windowing is applied versus not applied) is reflected in fixation and fixation recurrence (via RQA) patterns.*

  
  

## Why?

The goal of this code is to see whether we can infer something about the spatial dynamics of visual covert attention based on fixation and fixation recurrence patterns. Our hypothesis is that when we restrict the area of an image allowed to covert attention at each fixation (by applying a gaze-contingent window) that this will produce patterns in participants' fixation sequences and, more specifically, influence recurrence patterns of their fixations at different recurrence radii.

  

## How?

Here, we are applying an approach similar to the "decoding" style methods used to analyze neuroimaging data - training a feed-forward neural network on supervised regression and classification tasks, with the goal of predicting the size of the gaze-contingent window or whether windowing is applied from fixation recurrence patterns at different radii, calculated via RQA (see below).

  

If we can successfully predict the size of the "window", then conceptually, we have evidence that the size of the window impacts covert attentional dynamics in a manner evident in the fixation data.

  

Implemented in Python 3, using Tensorflow, Numpy/Scipy, Pandas, and scikit-learn/image, among other libraries.

  
  

## How might one use this code?

  

Run the included `start-exp-environment.sh` script, which will build a Docker container with everything you'll need to run the code for Experiment One and Experiment Two on the included initial datasets used for each. When the script is done, you'll be in a shell inside this docker container. From there, you have two options:
- `./run-experiment-1.sh`: Runs the analytical pipeline for Experiment One - predicting the size of a gaze-contingent window from fixation recurrence patterns
- `./run-experiment-2.sh`: Runs the analytical pipeline for Experiment Two - predicting whether windowing is indeed applied (versus a normal viewing condition)


Raw data used for Experiment One is included in the `raw-data-exp1` folder, and the raw data for Experiment Two is included (you guessed it) under `raw-data-exp2`. Processed data used during each run of the analytical pipeline (while the pipeline is running) is stored under `processed-data`. Finally, the results from each experiment are stored under `exp-results`. By default, each experiment runs the analytical pipeline on a new train-test split ten times. 

  
  

## RQA

Recurrence Quantification Analysis (RQA) is a method for analyzing recurrence patterns in dynamical systems (such as eye fixation sequences). The code for RQA (in `RqaPython/`) was developed by members of the [Brain, Attention, and Reality Lab](https://barlab.psych.ubc.ca/) at UBC. See the following citations for more info:

- Anderson, N. C., Bischof, W. F., Laidlaw, K. E. W., Risko, E. F., and Kingstone, A. (2013) Recurrence quantification analysis of eye movements. Behavior Research Methods, 45(3):842-56. doi: 10.3758/s13428-012-0299-5.

- Anderson, N. C., Anderson, F., Bischof, W. F., and Kingstone, A. (2014). A comparison of scanpath comparison methods, Behavior Research Methods, DOI 10.3758/s13428-014-0550-3.

- Wu, D. W. L., Anderson, N. C., Bischof, W. F., & Kingstone, A. (2014). Temporal dynamics of eye movements are related to differences in scene complexity and clutter. Journal of vision, 14(9), 8.

  

## Contact

Eli Kaplan - eli@kapln.org - [More of my work](https://kapln.org)

[Brain, Attention, and Reality Lab](https://barlab.psych.ubc.ca/research/)

  

Implemented for a UBC COGS 402 research project in Spring 2020. If this code is eventually used in a proper journal article, that will be linked here.