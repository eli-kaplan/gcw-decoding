# Dockerfile for gcw-decoding
# Defines an environment with all of the necessary pre-requisites to run this experiment code

FROM tensorflow/tensorflow:latest-gpu-py3-jupyter

# Install necessary system packages
RUN apt-get update && apt-get install -y \
        curl \
        libfreetype6-dev \
        libpng-dev \
        libzmq3-dev \
        pkg-config \
        python3-numpy \
        python3-pip \
        python3-scipy \
        git \
        libhdf5-dev \
        graphviz \
	build-essential \
	wget \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install necessary Python3 modules
RUN pip3 --no-cache-dir install \
        matplotlib \
        h5py \
        pydot-ng \
        graphviz \
	keras \
	pandas \
	sklearn \
        scikit-image

# Expose Jupyter Notebook port
EXPOSE 8888

USER 1000:1000

# Run Jupyter notebook
CMD ["/bin/bash", "-c", "cd /work; bash"]
