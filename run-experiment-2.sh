#!/usr/bin/env bash
# run-experiment-2.sh : Runs experiment 2 a specified number of times, storing results in exp-results/exp2-#.txt where # is the run number
NUM_EXPERIMENT_RUNS=10
EPOCHS_PER_RUN=15

# Clean up old experimental results
rm exp-results/exp2-*.txt 

# Run the experiment the specified number of times
echo "Running $NUM_EXPERIMENT_RUNS runs of experiment 2, training for $EPOCHS_PER_RUN epochs each trial."
for i in $(seq 1 $NUM_EXPERIMENT_RUNS)
do
    echo "Generating data for trial $i..."
    rm processed-data/exp2-* > /dev/null 2>&1
    ./data_gen_exp2.py > /dev/null 2>&1

    echo "Running prediction for trial $i - true labels..."
    echo "-- True Labels --" >> exp-results/exp2-$i.txt
    ./predict.py processed-data/exp2-categorical $EPOCHS_PER_RUN >> exp-results/exp2-$i.txt 2>/dev/null
    
    echo "Running prediction for trial $i - control (random) labels..."
    echo "-- Control Labels --" >> exp-results/exp2-$i.txt
    ./predict.py processed-data/exp2-categorical-validation $EPOCHS_PER_RUN >> exp-results/exp2-$i.txt 2>/dev/null
done
echo "All done! See exp-results/exp2-* for results from each run."