#!/usr/bin/env python3
"""
Program     data-gen-exp2.py
Purpose     Generating processed and train/test-split data sets from raw fixation sequence data (Experiment 2)
Author      Eli Kaplan
Date        Mar 2020
"""

from data_gen_common import FixationDataGenerator
import os
import random

class Exp2FixationDataGenerator(FixationDataGenerator):
    """ Class for processing raw fixation data and generating appropriately formatted data to plug into the network, for the paradigm of Experiment 2.
        Instead of predicting the window size, as in Experiment 1, here we are simply predicting whether the experimental condition is 1 (windowing) or 0 (not windowing). 
    """
    total_constrained_trials = 0        # Total number of constrained trials seen - used to generate summary
    total_unconstrained_trials = 0      # Total number of unconstrained trials seen

    def parse_data_from_file(self, filename):
        """ Loads raw trial data from a CSV file.

        Needs the following (case-insensitive) columns to be present in the target file:
            - condition
            - subject
            - current_fix_x
            - current_fix_y
            - trial_index

        Arguments:
            filename {string} -- File path
        
        """
        # First, load the raw file data, split by line
        raw_data = None
        try:
            with open(filename) as input_file:
                raw_data = input_file.readlines()
        
        except Exception as exc:
            print("Error loading file: " + str(exc))
            traceback.print_exc()
            sys.exit(1)

        # Acquire necessary column indices to parse data
        column_names = raw_data[0].rstrip().lower().split(',')
        fix_x_idx = None
        fix_y_idx = None
        trial_idx = None
        condition_idx = None
        subject_idx = None
        try:
            fix_x_idx = column_names.index('current_fix_x')
            fix_y_idx = column_names.index('current_fix_y')
            trial_idx = column_names.index('trial_index')
            condition_idx = column_names.index('condition')
            subject_idx = column_names.index('subject')

        except ValueError as exc:
            print("File {} is missing one of the necessary columns... {}".format(filename, str(exc)))
            sys.exit(1)

        if self.debug:
            print("Parsed indices in {}:\n"
                "\tcurrent_fix_x: {}\n"
                "\tcurrent_fix_y: {}\n"
                "\ttrial_index: {}\n"
                "\tcondition {}\n"
                "\tsubject {}\n"
                .format(filename, fix_x_idx, fix_y_idx, trial_idx, condition_idx, subject_idx))

        # Now, actually parse the data, storing fixations for each respective trial in the dataset
        trials = {}
        total_num_trials = 0
        total_num_fixations = 0
        unique_trial_ids = {}
        cur_unique_trial_id = 1
        num_zero_conditions = 0
        num_one_conditions = 0

        for raw_line in raw_data[1:]:
            line = raw_line.rstrip().split(',')

            # Retrieve each necessary column
            num_trial = int(line[trial_idx])
            num_subject = int(line[subject_idx])
            condition = line[condition_idx]
            fix_x = line[fix_x_idx]
            fix_y = line[fix_y_idx]

            # Since trial IDs are only unique for each subject in each file, we need to generate a new unique trial-subject pair ID, to be able to 
            # uniquely key each trial within each file. Hence, to do this robustly, we generate a unique key for each trial, as (100 * num_subject) + num_trial.
            # We then use this key to store an incrementing new trial ID in the unique_trial_ids dict. Each time we encounter a new trial, we add a new entry
            # in the unique_trial_ids dict, and increment the next ID to use. When we eventually process/store data, we will be using these per-file unique IDs instead
            # of the original trial/subject IDs, so there's no overlap.
            # NOTE(eli-kaplan): This necessarily limits each file to 99 subjects with 99 trials each. This will need to be edited if more of each are needed.
            unique_trial_id_idx = (num_subject * 100) + num_trial
            if unique_trial_id_idx not in unique_trial_ids:
                unique_trial_ids[unique_trial_id_idx] = cur_unique_trial_id
                cur_unique_trial_id += 1

            trial = unique_trial_ids[unique_trial_id_idx]

            # If this is the first point for this trial, initialize the structure
            if trial not in trials:
                if self.debug and total_num_trials % 20 == 0:
                    print("{}: {}...".format(filename, total_num_trials))

                trials[trial] = {}
                trials[trial]["window"] = condition # NOTE(eli-kaplan): Here, we are re-using the loaded 'window' field to refer to whether 
                                                    # the window is present (1 or 0), instead of the size of it. This value is later passed to gen_label() 
                trials[trial]["fixations"] = []
                total_num_trials += 1

                # Keep track of the number of trials encountered for each experimental condition
                if int(condition) == 1:
                    num_one_conditions += 1
                else:
                    num_zero_conditions += 1

            # Append the current fixation to the respective trial
            trials[trial]["fixations"].append([fix_x, fix_y])
            total_num_fixations += 1

        if self.debug:
            print("{}: Loaded {} trials ({} constrained, {} unconstrained), total of {} fixations.".format(
                filename, 
                total_num_trials, 
                num_one_conditions, num_zero_conditions,
                total_num_fixations))

        # Store the loaded data in this FixationDataGenerator instance, keyed by filename
        self.loaded_data[filename] = trials
        
        # Update the total numbers of each experimental condition encountered (across files)
        self.total_constrained_trials += num_one_conditions
        self.total_unconstrained_trials += num_zero_conditions

    def gen_label(self, label_data, label_type="categorical"):
        """ Processes/formats the label data for a given sample. In this experiment, we are only working with two-class data, so that's all that this
            function supports.
        
        Arguments:
            label_data {int} -- Label data i.e. experimental condition (1 or 0)
        
        Keyword Arguments:
            label_type {str} -- Type of output label to generate (here, only categorical supported) (default: {"categorical"})
        
        Raises:
            Exception: If invalid label type specified
        """
        label = None
        label_source = label_data

        # If we're generating random "control" labels intead of using the actual labels (for model validation), simply randomly pick 1 or 0 for the
        # labelled experimental condition.
        if self.random_control_labels:
            label_source = random.choices([0, 1])[0]

        # Simply, convert the 0 / 1 experimental condition into an exclusive two-class categorical label
        if label_type == "categorical":
            if int(label_source) == 1:
                label = [1, 0]
            else:
                label = [0, 1]

        else:
            raise Exception("Invalid label type specified")

        return label
    

# Command-line entrypoint
if __name__ == "__main__":
    # Initialize common parameters - change these if necessary.
    source_directory = "raw-data-exp2/"
    target_directory = "processed-data/"
    rqa_analytical_radii = [80,]

    common_rqa_params = {
        "linelength": 2,
        "mincluster": 8,
    }

    # Figure out which files we will be working with
    source_files = []
    for file in os.listdir(source_directory):
        if file.endswith(".csv"):
            source_files.append("{}{}".format(source_directory, file))

    if not source_files:
        print("No CSV source files found...")
        sys.exit(1)

    # Start the data generation process
    data_generator = Exp2FixationDataGenerator(source_files, rqa_analytical_radii, common_rqa_params, target_directory, debug=True)
    
    # Load all source data
    data_generator.load_all_source_data()

    # Display the "balance" of the dataset, for reference
    print("Total trials of each condition encountered: {} constrained, {} unconstrained.".format(
        data_generator.total_constrained_trials, data_generator.total_unconstrained_trials
    ))

    # Generate the actual-labelled dataset
    print("Generating actual-labeled data...")
    data_generator.process_all_data("exp2-categorical", label_type="categorical")
    
    # Generate the random-labeled "control" dataset
    print("Generating random-labeled 'control' dataset...")
    data_generator.random_control_labels = True
    data_generator.process_all_data("exp2-categorical-validation", label_type="categorical")