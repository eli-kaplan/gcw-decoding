#!/usr/bin/env python3
"""
Program     data-gen-exp1.py
Purpose     Generating processed and train/test-split data sets from raw fixation sequence data (Experiment 1)
Author      Eli Kaplan
Date        Apr 2020
"""

import os
import random
import numpy as np
from data_gen_common import FixationDataGenerator

class Exp1FixationDataGenerator(FixationDataGenerator):
    """ Class for processing raw fixation data and generating appropriately formatted data to plug into the network, for the paradigm of Experiment 1.
        Here, we are trying to predict the size of the gaze-contingent window - labelled either categorically (as exclusive classes) or continuously.

        NOTE(eli-kaplan): Data generation code here requires that trial IDs be unique between participants in the same file. Not a difficult modification to
                          remove this constraint - see data_gen_exp2.py.
    """

    possible_label_values = []      # Possible values for categorical labels (e.g. [30, 60, 90, ...]). Used both to generate categorical labels and to generate
                                    # random continuous labels holding values actually seen in the dataset.

    def parse_data_from_file(self, filename):
        """ Loads raw trial data from a CSV file

        Stores the parsed data in self.loaded_data to be further processed. 

        Needs the following (self-explanatory) columns to be present in target file:
            - window_size
            - TRIAL_INDEX
            - CURRENT_FIX_X
            - CURRENT_FIX_Y
        
        Arguments:
            filename {string} -- File path
        """

        # First, load the raw file data, split by line
        raw_data = None
        try:
            with open(filename) as input_file:
                raw_data = input_file.readlines()
        
        except Exception as exc:
            print("Error loading file: " + str(exc))
            traceback.print_exc()
            sys.exit(1)

        # Acquire necessary column indices to parse data
        column_names = raw_data[0].rstrip().split(',')
        fix_x_idx = None
        fix_y_idx = None
        trial_idx = None
        window_idx = None
        try:
            fix_x_idx = column_names.index('CURRENT_FIX_X')
            fix_y_idx = column_names.index('CURRENT_FIX_Y')
            trial_idx = column_names.index('TRIAL_INDEX')
            window_idx = column_names.index('window_size')

        except ValueError as exc:
            print("File {} is missing one of the necessary columns... {}".format(filename, str(exc)))
            sys.exit(1)

        if self.debug:
            print("Parsed indices in {}:\n"
                "\tCURRENT_FIX_X: {}\n"
                "\tCURRENT_FIX_Y: {}\n"
                "\tTRIAL_INDEX: {}\n"
                "\twindow_size: {}\n"
                .format(filename, fix_x_idx, fix_y_idx, trial_idx, window_idx))
        

        # Now, actually parse the data, storing fixations for each respective trial in the dataset
        trials = {}
        total_num_trials = 0
        total_num_fixations = 0

        for raw_line in raw_data[1:]:
            line = raw_line.rstrip().split(',')

            # Retrieve each necessary column
            trial = line[trial_idx]
            fix_x = line[fix_x_idx]
            fix_y = line[fix_y_idx]
            window = line[window_idx]

            # If this is the first point for this trial, initialize the structure
            if trial not in trials:
                if self.debug and total_num_trials % 20 == 0:
                    print("{}: {}...".format(filename, total_num_trials))

                trials[trial] = {}
                trials[trial]["window"] = window
                trials[trial]["fixations"] = []
                total_num_trials += 1

            # Append the current fixation to the respective trial
            trials[trial]["fixations"].append([fix_x, fix_y])
            total_num_fixations += 1

        if self.debug:
            print("{}: Loaded {} trials, total of {} fixations.".format(filename, total_num_trials, total_num_fixations))

        # Store the loaded data in this FixationDataGenerator instance, keyed by filename
        self.loaded_data[filename] = trials

    def gen_label(self, label_data, label_type="continuous"):
        """ Helper to generate a properly-formatted label (as a np.array) from given label data (i.e. window)
        
        Arguments:
            label_data {int or float} -- Raw label data (window size)
        
        Keyword Arguments:
            label_type {str} -- Label type (default: {"continuous"})
        
        Raises:
            Exception: If invalid label type specified
        
        Returns:
            np.array -- Processed label
        """
        label = None

        if label_type == "continuous":
            # For continuous labels, we can just create a 1x1 array with the label as the only value
            # For random "control" labels, pick a random possible window size
            if self.random_control_labels:
                label = np.array(random.choices(self.possible_label_values, k=1))
            # For true labels, take the actual window size
            else:
                label = np.array([label_data])
        
        elif label_type == "categorical":
            # Generate a categorical array with '1' at the index of the radius equal to the window, and '0' elsewhere.
            label = np.zeros(len(self.possible_label_values))

            # For random "control" labels, set a random index to 1.0
            if self.random_control_labels:
                label[np.array(self.possible_label_values) == int(random.choices(self.possible_label_values, k=1)[0])] = 1.0

            # Otherwise, set the index corresponding to the true label to 1.0
            else:
                label[np.array(self.possible_label_values) == int(label_data)] = 1.0

        else:
            raise Exception("Invalid label type")

        return label

# Command-line entrypoint
if __name__ == "__main__":
    # Initialize common parameters - change these if necessary.
    source_directory = "raw-data-exp1/"
    target_directory = "processed-data/"
    all_window_sizes = [x for x in range(70, 340, 30)]
    rqa_analytical_radii = all_window_sizes
    common_rqa_params = {
        "linelength": 2,
        "mincluster": 8,
    }

    # Figure out which files we will be working with
    source_files = []
    for file in os.listdir(source_directory):
        if file.endswith(".csv"):
            source_files.append("{}{}".format(source_directory, file))

    if not source_files:
        print("No CSV source files found...")
        sys.exit(1)

    # Start the data generation process
    data_generator = Exp1FixationDataGenerator(source_files, rqa_analytical_radii, common_rqa_params, target_directory, debug=True)
    data_generator.possible_label_values = all_window_sizes
    
    # Load all source data
    data_generator.load_all_source_data()

    # Process and output all data, using the true labels. Generates a random train-test split each time.
    data_generator.process_all_data("exp1-continuous", label_type="continuous")
    data_generator.process_all_data("exp1-categorical", label_type="categorical")

    # Generate the "control" dataset, where data frames are pulled from the same set (split randomized again) but labels are randomized.
    data_generator.random_control_labels = True
    data_generator.process_all_data("exp1-continuous-validation", label_type="continuous")
    data_generator.process_all_data("exp1-categorical-validation", label_type="categorical")