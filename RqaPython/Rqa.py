"""RQA and supporting functions
   Uses numpy, scipy.spatial.distance, scipy.stats, skimage.measure.
   """

import numpy as np
from scipy.spatial.distance import cdist
from scipy.stats import entropy
from skimage.measure import label, regionprops

# -----------------------------------------------------------------------------
# RQA
# -----------------------------------------------------------------------------

def Rqa(fixations, param):
    """Computes RQA of fixations
    result = Rqa(fixations, param)

    parameter:

    fixations             list of [x,y] fixations or
                          list of categories
    param['linelength']   threshold linelength, e.g. 2
    param['radius']       e.g. 64. For categories use 0.1
    param['mincluster']   e.g. 8, minimum size of clusters in recurrence matrix

    result:

    result['n']           n (length of fixations)
    result['recmat']      recurrence matrix
    result['nrec']        number of recurrences
    result['rec']         percentage of recurrences
    result['det']         determinism
    result['meanline']    meanline
    result['maxline']     maxline
    result['ent']         entropy
    result['relent']      relative entropy
    result['lam']         laminarity
    result['tt']          TT
    result['corm']        center of recurrence mass
    result['clusters']    number of recurrence clusters / nTriangle

    Author                Walter F. Bischof
    Date                  October 2018
    """

    # Default return values
    result = {}
    result['n'] = np.nan
    result['recmat'] = np.nan
    result['nrec'] = np.nan
    result['rec'] = np.nan
    result['det'] = np.nan
    result['revdet'] = np.nan
    result['meanline'] = np.nan
    result['maxline'] = np.nan
    result['ent'] = np.nan
    result['relent'] = np.nan
    result['lam'] = np.nan
    result['tt'] = np.nan
    result['corm'] = np.nan
    result['clusters'] = np.nan

    n = len(fixations)
    result['n'] = n

    if n <= 1:
        return result

    # Distance matrix

    if not isinstance(fixations[0], list):
        fixations = [[v] for v in fixations]
    distance_matrix = cdist(fixations, fixations)
    ntriangle = n * (n - 1) / 2

    # Recurrence matrix
    recurrence_matrix = (distance_matrix <= param['radius']) * 1
    result['recmat'] = recurrence_matrix.tolist()

    # Diagonal not needed for most measures
    partial_recurrence_matrix = np.triu(recurrence_matrix, 1)

    # Diagonals of recurrence matrix, excluding main diagonal
    recurrence_diagonals = [np.diag(recurrence_matrix, d) for d in range(1, n)]

    # Global measures
    nrec = np.sum(partial_recurrence_matrix)
    result['nrec'] = nrec
    result['rec'] = 100.0 * nrec / ntriangle

    # Diagonal line measures
    ndiagonals = n - 1
    thresholded_diagonals = []
    for diag in recurrence_diagonals:
        diag = np.r_[0, diag, 0]
        dchange = np.diff(diag)
        dlength = np.subtract(np.where(dchange == -1), np.where(dchange == 1))
        dlength = list(dlength[dlength >= param['linelength']])
        thresholded_diagonals.extend(dlength)

    if thresholded_diagonals:
        result['det'] = 100 * np.sum(thresholded_diagonals) / nrec
        result['meanline'] = np.mean(thresholded_diagonals)
        result['maxline'] = np.max(thresholded_diagonals)
        result['ent'], result['relent'] = compute_entropy(thresholded_diagonals, \
                param['linelength'], result['maxline'])
        corm = 0
        for i in range(ndiagonals):
            corm = corm + np.sum(recurrence_diagonals[i]) * (i + 1)
        corm = 100.0 * corm / ((n - 1) * nrec)
        result['corm'] = corm

    # Reverse det is like det but on the minor diagonal.
    # Revdet indicates a scanpath that is repeated in the opposite direction.
    flipped_partial_recurrence_matrix = np.flipud(np.triu(recurrence_matrix, 1))
    recurrence_diagonals = \
            [np.diag(flipped_partial_recurrence_matrix, d) for d in range(-n+1, n)]
    thresholded_diagonals = []
    for diag in recurrence_diagonals:
        diag = np.r_[0, diag, 0]
        dchange = np.diff(diag)
        dlength = np.subtract(np.where(dchange == -1), np.where(dchange == 1))
        dlength = list(dlength[dlength >= param['linelength']])
        thresholded_diagonals.extend(dlength)

    if thresholded_diagonals:
        result['revdet'] = 100 * np.sum(thresholded_diagonals) / nrec

    # horizontal+vertical line measures: laminarity and TT.
    # These are slightly different from Zbilut & Webber and Marwan
    # #(verticals + horizontals) in upper triangle =
    # #(verticals) in upper + #(verticals) in lower triangle
    # Set diagonal to zero to avoid counting over the diagonal

    thresholded_verticals = []
    rm = np.subtract(recurrence_matrix, np.eye(n))
    # In CRA, diagonal elements can be zero
    rm.clip(min=0)
    padded_rm = np.c_[np.zeros(n), rm, np.zeros(n)]
    for vertical in padded_rm:
        vchange = np.diff(vertical)
        vlength = np.subtract(np.where(vchange == -1), np.where(vchange == 1))
        vlength = list(vlength[vlength >= param['linelength']])
        thresholded_verticals.extend(vlength)

    if thresholded_verticals:
        result['lam'] = 100.0 * np.sum(thresholded_verticals) / np.sum(recurrence_matrix)
        result['tt'] = np.mean(thresholded_verticals)

    # Recurrence clusters
    result['clusters'] = \
        compute_recurrence_clusters(recurrence_matrix, param['mincluster'], ntriangle)

    return result

# -----------------------------------------------------------------------------
# Compute_entropy
# -----------------------------------------------------------------------------

def compute_entropy(a, min_length, max_length):
    """Computes entropy and relative entropy of length vectors."""

    _, counts = np.unique(a, return_counts=True)
    ent = entropy(counts, base=2)
    if min_length == max_length:
        rel_ent = np.nan
    else:
        rel_ent = ent / np.log2(max_length - min_length + 1)
    return ent, rel_ent

# -----------------------------------------------------------------------------
# Compute_recurrence_clusters
# -----------------------------------------------------------------------------

def compute_recurrence_clusters(recmat, threshold, ntriangle):
    """Compute number of recurrence clusters normalized by ntriangle
    see www.programcreek.com/python/example/88831/skimage.measure.regionprops
    """
    recmat = np.triu(recmat, 1)
    label_image = label(recmat, connectivity=2)
    total = 0
    for region in regionprops(label_image):
        if region.area >= threshold:
            total += region.area
    clusters = total * 100 / ntriangle
    return clusters
