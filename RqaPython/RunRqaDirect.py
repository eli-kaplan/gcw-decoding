#!/usr/bin/env python
"""
Program     TestRqa
Purpose     
Author      Walter F. Bischof
Date        September 2018
"""

import sys
import numpy
from Rqa import Rqa

# ----------------------------------------------------------------------
# Main
# ----------------------------------------------------------------------

def main():
    """Main Procedure"""

    # Filenames
    input_filename = "data/rqa-fixations.txt"

    # Parameters
    param = {}
    param['linelength'] = 2
    param['radius'] = 64
    param['mincluster'] = 8

    with open(input_filename,'r') as f:
        data = f.readlines()

    trial_number = []
    image_name = []
    fixation_x = []
    fixation_y = []
    fixation_dur = []

    for line in data[1:]:
        ele = line.strip().split()
        trial_number.append(int(float(ele[0])))
        image_name.append(ele[1])
        fixation_x.append(float(ele[2]))
        fixation_y.append(float(ele[3]))
        fixation_dur.append(int(float(ele[4])))

    print('trial\trec\tdet\tlam\ttt\tcorm\tent\trelent\tclusters')
    all_trial_numbers = list(set(trial_number))
    n = len(trial_number)
    for trial in all_trial_numbers:
        fixations = [[fixation_x[i], fixation_y[i]] for i in range(n) if trial_number[i] == trial]
        result = Rqa(fixations, param)
        line = '{:d}'.format(trial)
        fmt = '\t{:.2f}'*8
        line += fmt.format(result['rec'],result['det'],result['lam'], \
                result['tt'],result['corm'],result['ent'],result['relent'], \
                result['clusters'])
        print(line)

# ----------------------------------------------------------------------
# Program Start
# ----------------------------------------------------------------------

if __name__ == "__main__":
    main()

