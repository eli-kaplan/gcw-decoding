#!/usr/bin/env python3
"""
Program     predict.py
Purpose     Training and testing a feed-forward neural network on supervised regression and classification tasks - predicting GCW size and existence from
            fixation recurrence patterns.
Author      Eli Kaplan
Date        Apr 2020
"""
import os

import tensorflow as tf
import keras
from keras.layers import Dense, BatchNormalization, Activation, ActivityRegularization
from keras.layers import AveragePooling2D, Input, Flatten, Dropout
from keras.optimizers import Adam
from keras.regularizers import l2
from keras import backend as K
from keras.losses import mean_squared_error, mean_absolute_percentage_error, mean_absolute_error, categorical_crossentropy
from keras.metrics import categorical_accuracy, top_k_categorical_accuracy
from keras.models import Model

import numpy as np
import pathlib
from sklearn.preprocessing import *
from sklearn.metrics import classification_report, confusion_matrix

import json
import sys

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)


class DataLoader(keras.utils.Sequence):
    """ Data loader class - used to load processed data into memory for use in TensorFlow/Keras
    """
    data_by_id = {}
    data_meta = {}
    num_samples_loaded = 0 
    id_list = []
    shuffle_each_epoch = False
    batch_size = None
    dims = None
    label_dims = None
    indices = None
    label_type = "continuous"

    def __init__(self, data_path, type="train", batch_size=16, shuffle_each_epoch=True):
        """ Constructor - initializes data loader with given parameters
        
        Arguments:
            data_path {string} -- Path to data folder
        
        Keyword Arguments:
            type {str} -- Internal data frame group within dataset ('train' or 'test') (default: {"train"})
            batch_size {int} -- Size of each generated batch (default: {16})
            shuffle_each_epoch {bool} -- Whether to shuffle dataset each epoch (default: {True})
        """
        self.batch_size = batch_size
        self.shuffle_each_epoch = shuffle_each_epoch

        # Load metadata for this dataset
        data_meta_location = "{}/meta.json".format(data_path)
        with open(data_meta_location, "r") as data_meta_file:
            self.data_meta = json.loads(
                data_meta_file.read()
            )

        # Load all samples (specifically, train or test) from this dataset
        sample_ids_key = "{}_trials".format(type)
        for sample_id in self.data_meta[sample_ids_key]:
            self.data_by_id[sample_id] = []

            # Generate file paths for data frame and its corresponding label
            sample_path = "{}/{}.data.npy".format(data_path, sample_id)
            label_path = "{}/{}.label.npy".format(data_path, sample_id)

            # Load dataframe and label
            self.data_by_id[sample_id].append(
                np.load(sample_path)
            )

            self.data_by_id[sample_id].append(
                np.load(label_path)
            )
            
            # If we have not already determined input dimensions for this DataLoader, use this sample to do so.
            if not self.dims:
                self.dims = self.data_by_id[sample_id][0].shape
            
            # If we have not already determined _label_ dimensions for this DataLoader, do so here.
            if not self.label_dims:
                self.label_dims = self.data_by_id[sample_id][1].shape

            # Keep track of loaded samples
            self.num_samples_loaded += 1
            self.id_list = list(self.data_by_id.keys())

        # Determine whether we are in a categorical or continuous prediction paradigm
        if self.label_dims != (1,):
            self.label_type = "categorical"

        print("Loaded {} samples for type {}; {} labels.".format(self.num_samples_loaded, type, self.label_type))

        self.on_epoch_end()


    def __len__(self):
        """ Returns the number of batches loaded in this DataLoader
        
        Returns:
            int -- Number of batches
        """
        return int(np.floor(
            self.num_samples_loaded / self.batch_size
        ))

    def __getitem__(self, index):
        """ Retrieves one batch of data
        
        Arguments:
            index {int} -- Batch index (0..__len__)
        
        Returns:
            np.arr, np.arr -- Batch data, batch labels
        """
        batch_indices = self.indices[index * self.batch_size : (index + 1) * self.batch_size]

        X, y = self.generate_batch(batch_indices)

        return X, y


    def get_frame(self, idx):
        """ Helper function to retrieve a given frame by index
        
        Arguments:
            idx {int} -- Frame index
        
        Returns:
            np.arr, np.arr -- Frame data, Frame label
        """
        target_id = self.id_list[idx]
        file_data = self.data_by_id[target_id]

        X = file_data[0]    # Retrieve data frame
        y = file_data[1]    # Retrieve label

        return X, y

    def generate_batch(self, indices):
        """ Generates one batch of data
        
        Arguments:
            indices {int} -- Data (and label) indices to incorporate
        
        Returns:
            np.arr, np.arr -- Batch data, Batch labels
        """
        
        # Initialize empty arrays to hold batch data and labels
        batch_X = np.empty((self.batch_size, *self.dims))
        batch_y = np.empty((self.batch_size, *self.label_dims))

        # Iterate through and retrieve all requested frames
        store_idx = 0
        for idx in indices:
            frame_X, frame_y = self.get_frame(idx)

            batch_X[store_idx] = np.nan_to_num(frame_X.astype('float32'))
            batch_y[store_idx] = frame_y.astype('float32')

            store_idx += 1

        return batch_X, batch_y
    
    def on_epoch_end(self):
        """ Helper function triggered each epoch end - shuffles data, if requested.
        """
        self.indices = np.arange(self.num_samples_loaded)
        if self.shuffle_each_epoch:
            np.random.shuffle(self.indices)


class PredictRunner():
    """ Prediction runner class - loads data, initializes TF/Keras, and runs the model training and evaluation as requested.
    """
    train_generator = None
    test_generator = None
    data_dims = None
    label_dims = None
    label_type = "continuous"

    model = None

    def __init__(self, data_dir):
        """ Constructor - initializes training and test data DataLoaders, and retrieves data shape from them.
        
        Arguments:
            data_dir {[type]} -- [description]
        """
        self.train_generator = DataLoader(data_dir, batch_size = 8, shuffle_each_epoch = True)
        # NOTE(eli-kaplan): To easily calculate a confusion matrix, we need to not shuffle the test generator samples. This ensures
        # they stay in the same order when we retrieve their real labels, and calculate their predicted labels. 
        self.test_generator = DataLoader(data_dir, type="test", batch_size = 1, shuffle_each_epoch = False) 

        self.data_dims = self.train_generator.dims
        self.label_dims = self.train_generator.label_dims
        self.label_type = self.train_generator.label_type

        self.gen_model()


    def __del__(self):
        return

    def gen_model(self, 
        num_dense = 2,
        dense_elements = [32, 16],
        dense_dropouts = [0.25, 0.5],
        dense_activation = "tanh",
        batch_norm = True,
        optimizer = Adam()):
        """ Generates and compiles a Keras Model (i.e. neural network architecture) with specified parameters
        
        Keyword Arguments:
            num_dense {int} -- Number of dense (fully-connected) hidden layers (default: {2})
            dense_elements {list} -- Elements for each dense layer (default: {[32, 16]})
            dense_dropouts {list} -- Dropout values for each dense layer (default: {[0.25, 0.5]})
            dense_activation {str} -- Activation function for dense layers (default: {"tanh"})
            batch_norm {bool} -- Whether to apply batch normalization on dense layers (default: {True})
            optimizer {keras.Optimizer or str} -- Optimizer to use (default: {Adam()})
        """

        # Retrieve input shape and set some default parameters
        input_shape = self.data_dims
        output_activation = "linear"
        output_size = self.label_dims[0]
        loss_func = mean_absolute_percentage_error
        metrics = []

        # If we are performing a categorical prediction task, override paramters accordingly.
        if self.label_type == "categorical":
            output_activation = "softmax"
            loss_func = categorical_crossentropy
            metrics = [categorical_accuracy,]

        # Start the feed-forward model with an input layer
        input_layer = Input(shape = input_shape, name="model_input_layer")
        cur_layer = input_layer

        # Flatten the input layer to make things easier to work with
        cur_layer = Flatten()(cur_layer)

        # Iterate through and insert each requested dense layer
        for num in range(0, num_dense):
            # Add dense layer
            cur_layer = Dense(dense_elements[num], activation = None)(cur_layer)
            
            # If batch normalization requested, add BatchNorm layer
            if batch_norm:
                cur_layer = BatchNormalization()(cur_layer)

            # Add this dense layer's specified dropout layer
            cur_layer = Dropout(dense_dropouts[num])(cur_layer)

            # Finally, apply this dense layer's activation function
            cur_layer = Activation(dense_activation)(cur_layer)

        # Add a dense layer for the ouput, with the output's specified shape
        output_layer = Dense(output_size, activation = output_activation, name="model_output_layer")(cur_layer)

        # Generate the Model
        model = Model(inputs = input_layer, outputs = output_layer)

        # Compile the model with specified loss function, optimizer, and metrics
        model.compile(optimizer = optimizer, loss = loss_func, metrics = metrics)

        self.model = model

    def train(self, num_epochs = 5):
        """ Runs training phase on this neural model, with specified number of epochs
        
        Keyword Arguments:
            num_epochs {int} -- Number of training epochs to run (default: {5})
        """
        print("Training for {} epochs...".format(num_epochs))
        self.model.fit_generator(
            generator = self.train_generator,
            validation_data = self.test_generator,
            use_multiprocessing = True,
            workers = 4,
            epochs = num_epochs,
        )

    def test(self, test_data = None):
        """ Runs test/model evaluation on this neural model, with optionally specified test data generator
        
            If test data generator not specified, uses the currently-loaded one in this PredictionRunner

        Keyword Arguments:
            test_data {DataLoader} -- Test data source (default: {None})
        """
        print("Testing model...")
        if test_data is None:
            test_data = self.test_generator

        # Run model evaluation
        scores = self.model.evaluate_generator(test_data)

        # Print evaluation results according to model prediction task (categorical or continuous)
        if self.label_type == "continuous":
            print("Final mean absolute percentage error: {:.04f} percent.".format(scores))

        elif self.label_type == "categorical":
            print("Final categorical crossentropy: {:.04f}\nFinal categorical accuracy: {:.04f} percent.".format(scores[0], scores[1] * 100.0))

            # Perform in-depth validation for categorical models (not really possible for continuous)
            print("Evaluating model in-depth...")
            Y_pred = self.model.predict_generator(test_data)
            Y_true = []
            for idx in range(0, len(Y_pred)):
                Y_true.append(
                    np.mat(
                        test_data.get_frame(idx)[1]
                    )
                )

            num_classes = len(list(Y_pred[0]))
            Y_pred = np.argmax(Y_pred, axis=1)
            Y_true = np.argmax(np.squeeze(Y_true), axis=1)

            # Compose appropriate class names
            if num_classes == 2:
                target_names = ["Constrained", "Unconstrained"]
            else:
                target_names = [str(x) for x in test_data.data_meta["rqa_radii"]]

            # Display a classification report
            print("Classification report: ")
            print(classification_report(Y_true, Y_pred, target_names=target_names))

            # Display a raw confusion matrix
            print("Raw Confusion matrix: ")
            print(confusion_matrix(Y_true, Y_pred))

        else:
            print("Unknown label type - raw scores: {}".format(scores))
        

if __name__ == "__main__":
    """ Command-line entrypoint
    """
    if len(sys.argv) != 2 and len(sys.argv) != 3:
        print("Usage: predict.py <path to processed data> [num epochs]\n"
            "Data format (continuous or categorical) is determined automatically.\n"
            "Number of epochs defaults to 30 if not specified.")

        sys.exit(1)

    # Retrieve parameters from command line
    data_path = sys.argv[1]
    num_epochs = 30

    if len(sys.argv) == 3:
        num_epochs = int(sys.argv[2])

    # Initialize prediction runner
    runner = PredictRunner(data_path)
    
    # Run training phase
    runner.train(num_epochs = num_epochs)

    # Run evaluation phase
    runner.test()
